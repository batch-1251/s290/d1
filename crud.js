let http=require("http");
const PORT=3000;

let directory=[
	{"name":"Brandon", "Email":"brandon@mail.com"},
	{"name":"Robert", "Email":"robert@mail.com"}
	]

http.createServer((req, res)=>{
	if(req.url==="/users" && req.method==="GET")
	{
		res.writeHead(200,{"content-type":"application/json"});
		res.write(JSON.stringify(directory));
		res.end()
	}

	//create new user
		//url=/users
		//method=Post
		//data from the request will be coming from the client's body
		//req.body
		// console.log(req);
	if(req.url==="/users" && req.method==="POST")
	{
		let reqBody="";
		req.on("data", (data)=>{
			reqBody+=data;
		});
		req.on("end",()=>{
			console.log(typeof reqBody);
			reqBody=JSON.parse(reqBody);
			console.log(reqBody);
			let newUser={"name":reqBody.name, "email":reqBody.email}
			directory.push(newUser);
			console.log(directory);

		res.writeHead(200,{"content-type":"application/json"});
		res.write(JSON.stringify(directory));
		res.end();

		})
	}


}).listen(PORT);console.log(`Server is now connected to por ${PORT}`)